From mathcomp Require Import all_ssreflect all_algebra all_field.

Import Num.Theory GRing.Theory Num.Syntax.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicits Defensive.

Section the_exercise.

Variable R : rcfType.

Open Scope ring_scope.

Lemma sqrtrn_sqr n : n%:R = Num.sqrt((n * n)%:R) :> R.
Proof.
by apply/eqP; rewrite natrM -expr2 sqrtr_sqr eq_sym eqr_norm_id; apply: ler0n.
Qed.

Lemma sqrt2_lb : ratr (7%:Q / 5 %:Q) < Num.sqrt (ratr 2%:Q) :> R.
Proof.
rewrite -(@ltr_pexpn2r _ 2) -?topredE //= 1?ler0q ?sqrtr_ge0 //.
by rewrite -rmorphX /= sqr_sqrtr 1?ltr_rat 1?ler0q.
Qed.

Lemma sqrt3_ub : Num.sqrt (ratr 3%:Q) < ratr (26%:Q / 15%:Q) :> R.
Proof.
rewrite -(@ltr_pexpn2r _ 2) -?topredE //= 1?ler0q ?sqrtr_ge0 //.
by rewrite -rmorphX /= sqr_sqrtr 1?ltr_rat 1?ler0q.
Qed.

Lemma sqrt3_m_sqrt2_ub : Num.sqrt (ratr 3%:Q) - Num.sqrt (ratr 2%:Q) <
                                                ratr (1 / 3%:Q) :>R.
Proof.
rewrite (eqP (_ : 1 / 3%:Q == 26%:Q / 15%:Q - 7%:Q / 5 %:Q)) // rmorphB.
by rewrite ltr_add // ?ltr_opp2 ?sqrt2_lb ?sqrt3_ub.
Qed.

Lemma alternate_proof : 7%:R / 5%:R + 1/3%:R = 26%:R/ 15%:R :> R.
apply: (@mulIf _ 15%:R); first by rewrite (eqr_nat _ _ 0%N).
rewrite mulfVK; last by rewrite (eqr_nat _ _ 0%N).
rewrite mulrDl (_ : 15%:R = 5%:R * 3 %:R); last first.
  by rewrite -natrM.
rewrite mulrA mulfVK; last by rewrite (eqr_nat _ _ 0%N).
rewrite (mulrC (5%:R)) mulrA mulfVK // ?mul1r -?natrM -?natrD //.
by rewrite (eqr_nat _ _ 0%N).
Qed.

Lemma magnitude : 10%:R ^+ 910 < 3%:R ^+ 2012 :>R.
Proof.
have st2 : 10%:R ^+ 5 <= 3%:R ^+ 11 :> R.
  by rewrite -!natrX ler_nat.
rewrite -[X in 10%:R ^+ X]/(5 * 182)%N exprM.
have st3 : 10%:R ^+ 5 ^+ 182 <= 3 %:R ^+ 11 ^+ 182 :> R.
  by apply: ler_expn2r => //; apply/exprn_ge0/ler0n.
apply: (ler_lt_trans st3).
by rewrite -exprM ltr_eexpn2l ?(ltr_nat _ 1).
Qed.

Lemma main_trick (a b : R) (n : nat) :
  (a + b) ^+ (2 * n) + (a - b) ^+ (2 * n) =
  2%:R *
  \sum_(i < n.+1) a ^+ (2* n - 2 * i) * b ^+ (2 * i) *+ 'C(2 * n, 2 * i).
Proof.
set lhs := (LHS).
have step0 : lhs = 
    \sum_(i < (2 * n).+1) (a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i) +
           (-1) ^+ i * a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i)).
  by rewrite /lhs exprBn exprDn -big_split /=.
set fst_term := \sum_(i < (2 * n).+1 | odd i)
     (a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i) +
      (-1) ^+ i * a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i)).
have step1 : fst_term = 0.
  rewrite /fst_term; apply: big1 => i pi.
  rewrite -signr_odd pi /= expr1 -[X in X + _]mul1r -!(mulrA (-1)).
  by rewrite -!mulrnAr -mulrDl subrr mul0r.
have step2 : lhs = 
   fst_term +
  \sum_(i < (2 * n).+1 | ~~ odd i)
     (a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i) +
      (-1) ^+ i * a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i)).
  by rewrite step0 (bigID (fun i : 'I_(2 * n).+1 => odd i)) => /=.
move: step2; rewrite step1 add0r => step2.
have step3 : lhs = 2%:R * \sum_(i < (2 * n).+1 | ~~ odd i)
                     (a ^+ (2 * n - i) * b ^+ i *+ 'C(2 * n, i)).
  rewrite step2 big_distrr; apply: eq_bigr => i pi /=.
  by rewrite -signr_odd (negbTE pi) expr0 mul1r mulr2n mulrDl mul1r.
have hP : forall i : 'I_n.+1, (2 * i < (2 * n).+1)%N.
  move => i; rewrite ltnS leq_mul2l; apply/orP; right; rewrite -ltnS.
  by case:i.
set h := fun i => Ordinal (hP i).
have bijon : {on [pred i : 'I_(2 * n).+1| ~~ odd i], bijective h}.
  have hP' : forall i : 'I_(2 * n).+1, (i %/ 2 < n.+1)%N.
    move => i; rewrite ltnS [X in (_ <= X)%N](_ : (n = 2 * n %/ 2)%N);
    last by rewrite mulKn.
    by apply: leq_div2r; rewrite -ltnS; case: i.
  exists (fun i => Ordinal (hP' i)); move => i pi; apply val_inj => /=.
    by rewrite mulKn.
  by rewrite [RHS](divn_eq i 2) modn2 (negbTE pi) addn0 mulnC.
have step4 : lhs = 2%:R * (\sum_(j < n.+1 | ~~ odd (2 * j))
      a ^+ (2 * n - 2 * j) * b ^+ (2 * j) *+ 'C(2 * n, 2 * j)).
  by rewrite step3 (reindex h) //=.
by rewrite step4; congr (_ * _); apply/eq_big => // x; rewrite -dvdn2 dvdn_mulr.
Qed.

Lemma sqrt2_3_instance :
  exists M : nat, exists v, 0 < v < 1 /\
  (Num.sqrt (ratr 3%:Q) + Num.sqrt (ratr 2%:Q)) ^+ 2012 *+ 10 ^ 500 =
  10%:R *+ M + 9%:R + v :>R.
Proof.
have [[bign bignp]
      [dist distp]] :
   ((exists a, a = 1006%N) /\
    (exists a, a = 500%N)) by split; eapply ex_intro; reflexivity.  
have [[M Mp][v vp]] :
  (exists M : nat,
    M = (10 ^ (dist - 1) * 2 *
    (\sum_(i < bign.+1) 
          'C(2 * bign, 2 * i) * (2 ^ i * 3 ^ (bign - i))) - 1)%N) /\
   (exists v,
    v = 1 -
    (10 ^ dist)%:R *
        (Num.sqrt (ratr 3%:Q) - Num.sqrt (ratr 2%:Q)) ^+ (2 * bign) :> R).
  by split; eapply ex_intro; reflexivity.
have distpos : (0 < 10 ^ dist)%N by rewrite distp expn_gt0.
have bign2p : (2012 = 2 * bign)%N by reflexivity.
have intv : 0 < v < 1.
  have sqrt3m2p :  0 < (Num.sqrt (ratr 3%:~R) - Num.sqrt (ratr 2%:~R)) :> R.
     by rewrite subr_gt0 ltr_sqrt ?ltr_rat ?ltr0q.
  apply/andP; split; last first.
    rewrite -(subr0 1) vp ltr_add2l ltr_oppr opprK.
    apply: mulr_gt0; first by rewrite (ltr_nat _ 0%N).
    by apply: exprn_gt0.
  rewrite vp ltr_subr_addr add0r.
  apply: (@ltr_trans _ ((10 ^ dist)%:R / (10 ^ 910)%:R)); last first.
    rewrite distp [X in X / _](natrX R 10 500) [X in _ / X](natrX R 10 910).
    rewrite [X in _ < X](_ : _ = 10%:R ^+ 910 / 10%:R ^+ 910); last first.
      by rewrite mulfV ?expf_eq0 ?(eqr_nat _ _ 0%N).
    rewrite lter_pmul2r; last first.
      by rewrite invr_gt0 exprn_gt0 ?(ltr_nat _ 0%N).
    rewrite -[X in X < _]mulr1 -[910]/(500+410)%N exprD.
    rewrite lter_pmul2l; last by rewrite exprn_gt0 ?(ltr_nat _ 0).
    by rewrite exprn_egt1 ?(ltr_nat _ 1%N).
  rewrite lter_pmul2l; last by rewrite (ltr_nat _ 0).
  apply: (@ltr_trans R ((ratr (1 / 3%:Q)) ^+ (2 * bign))).
    rewrite ltr_pexpn2r.
          by apply: sqrt3_m_sqrt2_ub.
        by rewrite muln_gt0 /= bignp.
      by rewrite /Num.nneg /= -topredE /= ltrW.
    by rewrite /Num.nneg /= -topredE /= ler0q.
  rewrite rmorphM /= rmorph1 mul1r fmorphV exprVn /=.
  rewrite ltf_pinv.
      by rewrite bignp ratr_int natrX magnitude.
    by rewrite -topredE /= exprn_gt0 // ltr0q (ltr_int _ 0).
  by rewrite -topredE /= natrX exprn_gt0 // (ltr_nat _ 0).
have [[A Ap][B Bp]] :
 (exists A,
     A = (Num.sqrt (ratr 3%:R) + Num.sqrt (ratr 2%:R)) ^+ (2 * bign) :> R) /\
 (exists B, B = (Num.sqrt (ratr 3%:R) - Num.sqrt (ratr 2%:R)) ^+ (2 * bign)
   :> R).
  by split; eapply ex_intro; reflexivity.
have behold_digit : A *+ 10 ^ dist = 10%:R *+ M + 9%:R + v.
  rewrite -(addrK B A) mulrnBl -[RHS](addrA _ _ v) -(addrK 1 9%:R).
  rewrite -(addrC 1) -[1 + 9%:R]/(10%:R) -(addrA 10%:R) [RHS]addrA.
  have map_small_values : B *- 10 ^ dist = -1 + v.
    by rewrite vp addKr mulr_natl Bp.
  congr (_ + _); last by [].
  rewrite [RHS](_ : _ = (M + 1)%:R * 10%:R); last first.
    by rewrite natrD mulrDl mul1r mulr_natl.
  rewrite Mp subnK; last first.
    rewrite 2!muln_gt0 expn_gt0 /= big_ord_recl addn_gt0; apply/orP; left.
    by rewrite /= muln0 bin0 expn0 !mul1n subn0 expn_gt0.
  rewrite -natrM -(mulnC 10) 2!mulnA -expnS -mulnA natrM mulr_natl.
  congr (_ *+ _ ^ _); last by rewrite distp.
  rewrite Ap Bp main_trick natrM; congr (_ * _).
  rewrite (@big_morph R nat (fun n => n%:R) 0 +%R) /=;[ | | by []]; last first.
    by move => x y; rewrite natrD.
  apply: eq_bigr => i _.
  rewrite natrM mulr_natl; congr (_ *+ _).
  rewrite mulrC natrM !natrX; congr (_ * _).
    by rewrite exprM sqr_sqrtr ?ler0q // ratr_nat.
  by rewrite !(mulnC 2) -mulnBl -(mulnC 2) exprM sqr_sqrtr ?ler0q // ratr_nat.
exists M; exists v; split; first by exact intv.
by move: behold_digit; rewrite Ap bignp distp.
Qed.
